#include <ros/ros.h>
#include <sensor_msgs/Image.h>

ros::Publisher pub;


void camMsgRecv(const sensor_msgs::Image& cam)
{
  pub.publish(cam);
}

int main(int argc, char* argv[]) {
	ros::init(argc, argv, "my_camera_node");
	ros::NodeHandle h;
	ros::Subscriber sub = h.subscribe("camera", 1000, &camMsgRecv);
  pub = h.advertise<sensor_msgs::Image>("my_camera_node/my_new_camera_topic", 10);

  ros::spin();

  return 0;
}
