#include <stdlib.h>
#include <string>

int main(int argc, char* argv[]) {
  std::string command = "rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '{linear: {x: 1.2, y: 0.0, z: 0.0}, angular: {x: 0.0,y: 0.0,z: 1.1}}'";
  for (int i = 0; i < 6; i++) {
    system(command.c_str());
  }

  return 0;
}
