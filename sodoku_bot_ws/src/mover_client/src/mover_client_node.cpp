#include <math.h>
#include <fstream>
#include <ros/ros.h>
#include <mover_client/grid_num.h>
#include <mover_client/grid_num_vector.h>
#include <move_arm_joints/move_and_confirm.h>

const int GRID_SIZE = 100;
const char CSV_DELIM = ',';
// Global service clients so that they can be used in the callbacks
ros::ServiceClient armShoulderPan;
ros::ServiceClient armElbowFlex;
ros::ServiceClient armWristFlex;
ros::ServiceClient armShoulderLift;
ros::ServiceClient gripper;

struct CSVLine {
  int row;
  int col;
  int num;
  double arm_shoulder_pan;
  double arm_elbow_flex;
  double arm_wrist_flex;
  double arm_shoulder_lift;
  double gripper;
};

// This is global so that we don't have to rebuild it over and over again
std::vector<std::vector<std::vector<CSVLine>>> CSVArray;


void buildCSVArray() {
  // Build the indices for the array
  for (int row = 0; row < GRID_SIZE; row++) {
    std::vector<std::vector<CSVLine>> tmpvv;
    CSVArray.push_back(tmpvv);
    for (int col = 0; col < GRID_SIZE; col++) {
      std::vector<CSVLine> tmpv;
      CSVArray[row].push_back(tmpv);
      for (int num = 0; num < GRID_SIZE; num++) {
        CSVLine tmp;
        CSVArray[row][col].push_back(tmp);
      }
    }
  }

  // Grab the file stream
  std::ifstream csvf;
  csvf.open("param.csv");
  // Skip the first line
  std::string s;
  std::getline(csvf, s);

  // This does not use constants because the file is static
  for (int i = 0; i < 1000000; i++) {
    CSVLine l;

    if (std::getline(csvf, s, CSV_DELIM)) {
      l.row = std::stoi(s);
    }

    if (std::getline(csvf, s, CSV_DELIM)) {
      l.col = std::stoi(s);
    }

    if (std::getline(csvf, s, CSV_DELIM)) {
      l.num = std::stoi(s);
    }

    if (std::getline(csvf, s, CSV_DELIM)) {
      l.arm_shoulder_pan = std::stod(s);
    }

    if (std::getline(csvf, s, CSV_DELIM)) {
      l.arm_elbow_flex = std::stod(s);
    }

    if (std::getline(csvf, s, CSV_DELIM)) {
      l.arm_wrist_flex = std::stod(s);
    }

    if (std::getline(csvf, s, CSV_DELIM)) {
      l.arm_shoulder_lift = std::stod(s);
    }

    if (std::getline(csvf, s, '\n')) {
      l.gripper = std::stod(s);
    }

    CSVArray[l.row - 1][l.col - 1][l.num - 1] = l;
  }

  csvf.close();
}


void solution_received(const mover_client::grid_num_vector &sol) {
  const int solLen = GRID_SIZE * GRID_SIZE;

  for (int i = 0; i < solLen; i++) {
    mover_client::grid_num curr = sol.numbered_grids[i];

    // Decode the appropriate movements from the CSV file
    CSVLine movements = CSVArray[curr.row][curr.col][curr.num];

    // Send movements to each service and check the response
    move_arm_joints::move_and_confirm::Request req;
    req.move = movements.arm_shoulder_pan;
    move_arm_joints::move_and_confirm::Response resp;
    resp.confirm = -0.125;
    while (resp.confirm != req.move) {
      armShoulderPan.call(req, resp);
      ROS_INFO_STREAM("armShoulderPan moving to " << req.move << " Sensor reads " << resp.confirm);
    }

    req.move = movements.arm_elbow_flex;
    resp.confirm = -0.125;
    while (resp.confirm != req.move) {
      armElbowFlex.call(req, resp);
      ROS_INFO_STREAM("armElbowFlex moving to " << req.move << " Sensor reads " << resp.confirm);
    }

    req.move = movements.arm_wrist_flex;
    resp.confirm = -0.125;
    while (resp.confirm != req.move) {
      armWristFlex.call(req, resp);
      ROS_INFO_STREAM("armWristFlex moving to " << req.move << " Sensor reads " << resp.confirm);
    }

    req.move = movements.arm_shoulder_lift;
    resp.confirm = -0.125;
    while (resp.confirm != req.move) {
      armShoulderLift.call(req, resp);
      ROS_INFO_STREAM("armShoulderLift moving to " << req.move << " Sensor reads " << resp.confirm);
    }

    req.move = movements.gripper;
    resp.confirm = -0.125;
    while (resp.confirm != req.move) {
      gripper.call(req, resp);
      ROS_INFO_STREAM("gripper moving to " << req.move << " Sensor reads " << resp.confirm);
    }
  }
}


int main(int argc, char* argv[]) {
  ros::init(argc, argv, "mover_client_node");
  ros::NodeHandle h;
  ros::Subscriber subHdlr = h.subscribe("num_grid_sol/vector", 1000, &solution_received);
  // The 5 service clients
  armShoulderPan = h.serviceClient<move_arm_joints::move_and_confirm>("arm_shoulder_pan_joint_service");
  armElbowFlex = h.serviceClient<move_arm_joints::move_and_confirm>("arm_elbow_flex_joint_service");
  armWristFlex = h.serviceClient<move_arm_joints::move_and_confirm>("arm_wrist_flex_joint_service");
  armShoulderLift = h.serviceClient<move_arm_joints::move_and_confirm>("arm_shoulder_lift_joint_service");
  gripper = h.serviceClient<move_arm_joints::move_and_confirm>("gripper_service");

  buildCSVArray();

  ros::spin();

  return 0;
}
