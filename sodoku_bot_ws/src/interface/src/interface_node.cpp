#include <ros/ros.h>
#include <mover_client/grid_num.h>
#include <mover_client/grid_num_vector.h>

#include <string>
#include <fstream>
#include <stdlib.h>

ros::Publisher resultsPub;

void newInputBoard(const mover_client::grid_num_vector &board) {
  int len = sizeof(board.numbered_grids) / sizeof(board.numbered_grids[0]);

  std::ofstream factsFile;
  factsFile.open("my_sudoku_facts.lp");

  for (int i = 0; i < len; i++) {
    factsFile << "gridNum(" << board.numbered_grids[i].row << ',' << board.numbered_grids[i].col << ',' << board.numbered_grids[i].num << ").\n";
  }

  factsFile.close();

  system("clingo my_sudoku_facts.lp my_sudoku_ASP_program.lp | head -n5 | tail -n1 > result.txt");

  std::ifstream results;
  results.open("result.txt");

  // Now decode each result into an array
  mover_client::grid_num_vector resultsArr;
  std::string s;

  while (std::getline(results, s, '(')) {
    mover_client::grid_num curr;

    if (std::getline(results, s, ',')) {
      curr.row = std::stoi(s);
    }

    if (std::getline(results, s, ',')) {
      curr.col = std::stoi(s);
    }

    if (std::getline(results, s, ')')) {
      curr.num = std::stoi(s);
    }

    resultsArr.numbered_grids.push_back(curr);
  }

  // Finally publish the results
  resultsPub.publish(resultsArr);
}


int main(int argc, char* argv[]) {
  ros::init(argc, argv, "interface_node");
  ros::NodeHandle h;
  ros::Subscriber inputSub = h.subscribe("num_grid/vector", 1000, &newInputBoard);
  resultsPub = h.advertise<mover_client::grid_num_vector>("num_grid_sol/vector", 1000);

  ros::spin();

  return 0;
}
