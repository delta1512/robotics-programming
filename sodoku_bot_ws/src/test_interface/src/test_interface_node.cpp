#include <ros/ros.h>
#include <mover_client/grid_num.h>
#include <mover_client/grid_num_vector.h>

#include <string>

const std::string facts = "1,1,5 \
1,4,1 \
1,5,2 \
1,7,6 \
1,9,4 \
2,6,8 \
2,7,2 \
2,8,9 \
2,9,3 \
3,4,4 \
3,8,7 \
4,3,2 \
4,4,6 \
4,5,8 \
4,6,5 \
4,7,4 \
4,8,1 \
5,1,8 \
5,3,5 \
5,5,7 \
5,7,9 \
5,9,6 \
6,2,6 \
6,3,7 \
6,4,9 \
6,5,4 \
6,6,2 \
6,7,3 \
7,2,7 \
7,6,6 \
8,1,2 \
8,2,8 \
8,3,6 \
8,4,7 \
9,1,3 \
9,3,1 \
9,5,9 \
9,6,4 \
9,9,2";


int main(int argc, char* argv[]) {
  ros::init(argc, argv, "test_interface_node");
  ros::NodeHandle h;
  ros::Publisher pub = h.advertise<mover_client::grid_num_vector>("num_grid/vector", 1000);

  // Decode each fact into an array
  mover_client::grid_num_vector resultsArr;
  std::stringstream ss;
  ss << facts;
  std::string s;

  while (std::getline(ss, s, ',')) {
    mover_client::grid_num curr;

    curr.row = std::stoi(s);

    if (std::getline(ss, s, ',')) {
      curr.col = std::stoi(s);
    }

    if (std::getline(ss, s, ' ')) {
      curr.num = std::stoi(s);
    }

    resultsArr.numbered_grids.push_back(curr);
  }

  mover_client::grid_num_vector msg = resultsArr;

  ROS_INFO_STREAM("Test is starting in a few seconds...");

  ros::Rate rate(0.25);
  while (ros::ok()) {
    pub.publish(msg);
    ros::spinOnce();
    rate.sleep();
  }

  return 0;
}
