#include <ros/ros.h>
#include <move_arm_joints/move_and_confirm.h>

#include <string>
#include <fstream>
#include <stdlib.h>


bool mover_service(move_arm_joints::move_and_confirm::Request &req,
                  move_arm_joints::move_and_confirm::Response &resp) {
  // Run the command based on the input
  std::string command = "arm_shoulder_pan_servo " + std::to_string(req.move);
  ROS_DEBUG_STREAM("Calling command " << command);
  system(command.c_str());

  // Read the log file for the sensor value
  std::ifstream logFile;
  logFile.open("arm_shoulder_pan_servo_confirm.log");

  // Get the last line of the log file
  std::string logLine, prevLogLine;
  do {
    prevLogLine = logLine;
  } while (std::getline(logFile, logLine));

  ROS_DEBUG_STREAM("Log file reads: " << prevLogLine);

  // Convert it to a double
  double sensorVal = std::stod(prevLogLine);

  logFile.close();

  resp.confirm = sensorVal;

  return true;
}


int main(int argc, char* argv[]) {
  ros::init(argc, argv, "arm_shoulder_pan_joint_node");
  ros::NodeHandle h;
  ros::ServiceServer server = h.advertiseService("arm_shoulder_pan_joint_service", &mover_service);

  ros::Rate rate(1);
  while (ros::ok()) {
    ros::spinOnce();
    rate.sleep();
  }

  return 0;
}
