This software assumes that all servo executables are in a $PATH directory whereas all of the sensors are in devel/lib/move_arm_joints/.

Multiple options for the ASP solver are provided and to change the one currently used, move it to devel/lib/interface/.

The param.csv file must be located in devel/lib/mover_client/.

You can run this by executing:
source devel/setup.bash
roslaunch test_interface run_assignment.launch
