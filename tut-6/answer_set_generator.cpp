/*
Example compilation and usage:

Compilation: g++ answer_set_generator.cpp

Usage: ./a.out > answer_set.txt; ./zchaff answer_set.txt | ./a.out 1

The additional parameter will tell the program to read the output of zchaff and translate it
*/

#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
#include <assert.h>
#include <stdlib.h>
#include <string.h>

#define BOARD_SIZE 9
#define SUBGRID_SIZE 3
#define STR_COMMA "_"
#define STR_NEGATION "-"
// defs for managing hash space
#define MAX_IDX (2*BOARD_SIZE*BOARD_SIZE + BOARD_SIZE*BOARD_SIZE*BOARD_SIZE + BOARD_SIZE*SUBGRID_SIZE*SUBGRID_SIZE + 2)
#define HASH_SP_SB_START (2 * BOARD_SIZE * BOARD_SIZE + 1)
#define HASH_SP_G_START (HASH_SP_SB_START + SUBGRID_SIZE * SUBGRID_SIZE * BOARD_SIZE + 1)


std::string hard_code = "G_1_1_5\n\
G_1_4_1\n\
G_1_5_2\n\
G_1_7_6\n\
G_1_9_4\n\
G_2_6_8\n\
G_2_7_2\n\
G_2_8_9\n\
G_2_9_3\n\
G_3_4_4\n\
G_3_8_7\n\
G_4_3_2\n\
G_4_4_6\n\
G_4_5_8\n\
G_4_6_5\n\
G_4_7_4\n\
G_4_8_1\n\
G_5_1_8\n\
G_5_3_5\n\
G_5_5_7\n\
G_5_7_9\n\
G_5_9_6\n\
G_6_2_6\n\
G_6_3_7\n\
G_6_4_9\n\
G_6_5_4\n\
G_6_6_2\n\
G_6_7_3\n\
G_7_2_7\n\
G_7_6_6\n\
G_8_1_2\n\
G_8_2_8\n\
G_8_3_6\n\
G_8_4_7\n\
G_9_1_3\n\
G_9_3_1\n\
G_9_5_9\n\
G_9_6_4\n\
G_9_9_2";


struct var_index {
  static int object_count; //use object counter as index
  std::string name;
  bool negation;
  int index;

  var_index(bool neg, std::string var, int n1, int n2, int n3=-1) {
    negation = neg;
    if (neg) {
      name = STR_NEGATION;
    } else {
      name = "";
    }

    name = "";

    name += var;
    name += STR_COMMA + std::to_string(n1) + STR_COMMA;
    name += std::to_string(n2);
    if (n3 > 0) {
      name += STR_COMMA + std::to_string(n3);
    }

    assert(n3 == -1 || (n3 > 0 && n3 <= BOARD_SIZE));

    // Calculate the index based on the parameters provided
    /*
    This implements a hash space that ensures that any name provided can be mapped
    uniquely to a particular number between 1 and MAX_IDX
    */
    if (var == "SB") {
      index = (n3-1) + ((n2-1) * BOARD_SIZE) + ((n1-1) * BOARD_SIZE * SUBGRID_SIZE) + HASH_SP_SB_START;
    } else if (var == "G") {
      index = (n3-1) + ((n2-1) * BOARD_SIZE) + ((n1-1) * BOARD_SIZE * BOARD_SIZE) + HASH_SP_G_START;
    } else {
      int i1 = 1;
      if (var == "R") {
        i1 = 0;
      }

      index = (n2-1) + ((n1-1) * BOARD_SIZE) + (i1 * BOARD_SIZE * BOARD_SIZE);
    }

    index++; // Shifts the index so that it aligns with 1
    // FOR TESTING PURPOSES
    /*
    if (index > MAX_IDX) {
      std::cout << "max " << MAX_IDX << std::endl;
      std::cout << var << " " << n1 << " " << n2 << " " << n3 << std::endl;
      std::cout << index << std::endl;
    }
    */
    assert(index <= MAX_IDX);
  }

  std::string qualified_index() {
    if (negation) {
      return STR_NEGATION + std::to_string(index);
    } else {
      return std::to_string(index);
    }
  }
};


// Note that the boolean casts here are to counteract C++'s method of integer division
// for edge cases where the numbers are exactly divisible
std::string reverse_hash_func(int hash) {
  int pos_hash = abs(hash);
  int n1, n2, n3, normalised;

  if (pos_hash >= HASH_SP_G_START) {
    normalised = pos_hash - HASH_SP_G_START;
    int coeff = (int) (normalised % (BOARD_SIZE * BOARD_SIZE) == 0);
    normalised -= coeff;

    n1 = normalised / (BOARD_SIZE * BOARD_SIZE);
    normalised = normalised % (BOARD_SIZE * BOARD_SIZE);
    n2 = (normalised / BOARD_SIZE) - (int) (normalised % BOARD_SIZE == 0);
    n3 = (normalised-1) % BOARD_SIZE;

    return "G_" + std::to_string(n1+1) + "_" + std::to_string(n2+1) + "_" + std::to_string(n3+1+coeff);

  } else if (pos_hash >= HASH_SP_SB_START) {
    normalised = pos_hash - HASH_SP_SB_START;
    int coeff = (int) (normalised % (BOARD_SIZE * SUBGRID_SIZE) == 0);
    normalised -= coeff;

    n1 = normalised / (BOARD_SIZE * SUBGRID_SIZE);
    int nnormalised = normalised % (BOARD_SIZE * SUBGRID_SIZE);
    n2 = (nnormalised / BOARD_SIZE) - (int) (nnormalised % BOARD_SIZE == 0);
    n3 = (nnormalised-1) % BOARD_SIZE;

    return "SB_" + std::to_string(n1+1) + "_" + std::to_string(n2+1) + "_" + std::to_string(n3+1+coeff);

  } else {
    normalised = pos_hash;
    std::string letter = "R";

    if (pos_hash > BOARD_SIZE * BOARD_SIZE) {
      normalised = pos_hash - BOARD_SIZE * BOARD_SIZE;
      letter = "C";
    }

    n2 = (normalised-1) % BOARD_SIZE;
    // Might need to reduce normalisation here
    n1 = (normalised / BOARD_SIZE) - (int) (normalised % BOARD_SIZE == 0);

    return letter + "_" + std::to_string(n1+1) + "_" + std::to_string(n2+1);
  }
}


void print_to_stdout(std::vector<std::vector<var_index>>& var_vect, int n_vars) {
  std::cout << "p cnf " << MAX_IDX << " " << var_vect.size() << std::endl;

  // for every clause
  for (int i = 0; i < var_vect.size(); i++) {
    // for every atom
    for (int j = 0; j < var_vect[i].size(); j++) {
      /*
      if (strcmp(reverse_hash_func(var_vect[i][j].index).c_str(), var_vect[i][j].name.c_str())) {
        std::cout << reverse_hash_func(var_vect[i][j].index) << std::endl;
        std::cout << var_vect[i][j].name << " " << var_vect[i][j].index << std::endl;
        assert(false);
      }
      */
      std::cout << var_vect[i][j].qualified_index() << " ";
      //std::cout << var_vect[i][j].name << " ";
    }
    std::cout << '0' <<  std::endl;
  }
}


int main(int argc, char** argv) {
  /*
  std::cout << "HASH_SP_SB_START " << HASH_SP_SB_START << std::endl;
  std::cout << "HASH_SP_G_START " << HASH_SP_G_START << std::endl;
  */

  if (argc > 1) { // Then we have switched into reading mode
    std::string stdin;
    // The zchaff solution is on line 4 after 12 spaces/newlines, so skip them
    for (int i = 0; i < 12; i++) {
      std::cin >> stdin;
    }

    // read in and convert everything
    do {
      if (stdin[0] == '-') {
        std::cout << '-';
      }

      std::cout << reverse_hash_func(std::stoi(stdin)) << std::endl;

      std::cin >> stdin;
    } while (stdin != "Max");

    return 0;
  }

  // This is an array of clauses (which are arrays of disjunctions)
  std::vector<std::vector<var_index>> var_vect;
  int n_vars = 0;

  // Formula 1
  for (int i = 1; i < BOARD_SIZE + 1; i++) {
    for (int j = 1; j < BOARD_SIZE + 1; j++) {
      std::vector<var_index> clause1, clause2;
      var_index var_R(false, "R", i, j);
      var_index var_C(false, "C", i, j);

      clause1.push_back(var_R);
      clause2.push_back(var_C);

      var_vect.push_back(clause1);
      var_vect.push_back(clause2);

      n_vars += 3;
    }
  }

  for (int sr = 1; sr < SUBGRID_SIZE + 1; sr++) {
    for (int sc = 1; sc < SUBGRID_SIZE + 1; sc++) {
      for (int k = 1; k < BOARD_SIZE + 1; k++) {
        std::vector<var_index> clause3;
        var_index var_SB(false, "SB", sr, sc, k);
        clause3.push_back(var_SB);
        var_vect.push_back(clause3);
      }
    }
  }

  // Formula 2
  for (int i = 1; i < BOARD_SIZE + 1; i++) {
    for (int j = 1; j < BOARD_SIZE + 1; j++) {
      std::vector<var_index> clause;
      var_index var_R(true, "R", i, j);
      clause.push_back(var_R);
      n_vars++;

      for (int g = 1; g < BOARD_SIZE + 1; g++) {
        var_index var(false, "G", i, g, j);
        clause.push_back(var);
        n_vars++;
      }

      var_vect.push_back(clause);
    }
  }

  // Formula 3
  for (int i = 1; i < BOARD_SIZE + 1; i++) {
    for (int j = 1; j < BOARD_SIZE + 1; j++) {
      std::vector<var_index> clause;
      var_index var_R(true, "C", i, j);
      clause.push_back(var_R);
      n_vars++;

      for (int g = 1; g < BOARD_SIZE + 1; g++) {
        var_index var(false, "G", g, i, j);
        clause.push_back(var);
        n_vars++;
      }

      var_vect.push_back(clause);
    }
  }

  // Formula 4
  for (int sr = 0; sr < SUBGRID_SIZE; sr++) {
    for (int sc = 0; sc < SUBGRID_SIZE; sc++) {
      for (int k = 1; k < BOARD_SIZE + 1; k++) {
        std::vector<var_index> clause;
        var_index var_SB(true, "SB", sr + 1, sc + 1, k);
        clause.push_back(var_SB);
        n_vars++;

        // Now create all the var_indexes for each subgrid
        for (int r = 1; r < SUBGRID_SIZE + 1; r++) {
          for (int c = 1; c < SUBGRID_SIZE + 1; c++) {
            var_index var(false, "G", r + (sr * SUBGRID_SIZE), c + (sc * SUBGRID_SIZE), k);
            clause.push_back(var);
            n_vars++;
          }
        }

        var_vect.push_back(clause);
      }
    }
  }

  // Formula 5
  for (int i = 1; i < BOARD_SIZE + 1; i++) {
    for (int j = 1; j < BOARD_SIZE + 1; j++) {
      for (int k = 1; k < BOARD_SIZE + 1; k++) {
        for (int l = 1; l < BOARD_SIZE + 1; l++) {
          if (k == l) {
            continue;
          }
          std::vector<var_index> clause;
          var_index var1(true, "G", i, j, k);
          var_index var2(true, "G", i, j, l);
          clause.push_back(var1);
          clause.push_back(var2);
          n_vars += 2;

          var_vect.push_back(clause);
        }
      }
    }
  }

  // Handle hardcodes
  // + 1 for missing \n at the end
  int n_hardcodes = std::count(hard_code.begin(), hard_code.end(), '\n') + 1;
  for (int i = 0; i < n_hardcodes; i++) {
    char varc = hard_code[i*8+0];
    std::string var(1, varc);
    int n1 = (int)hard_code[i*8+2] - 48;
    int n2 = (int)hard_code[i*8+4] - 48;
    int n3 = (int)hard_code[i*8+6] - 48;
    std::vector<var_index> clause;
    var_index hard_var(false, var, n1, n2, n3);
    clause.push_back(hard_var);

    var_vect.push_back(clause);
  }

  print_to_stdout(var_vect, n_vars);

  return 0;
}
