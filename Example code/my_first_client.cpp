#include <ros/ros.h>
#include<turtlesim/Spawn.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
	ros::init(argc, argv, "my_client_node");
	ros::NodeHandle nh;
	ros::ServiceClient myClient = nh.serviceClient<turtlesim::Spawn>("spawn");
	turtlesim::Spawn::Request req;
	turtlesim::Spawn::Response resp;
	req.x = 2;
	req.x = 3;
	req.theta = M_PI / 2;
	req.name = "Leo";
	
	bool success = myClient.call(req, resp);

	if(success)
	{
		ROS_INFO_STREAM("Spawned a turtle named " << resp.name);
	}
	else
	{
		ROS_INFO_STREAM("Failed to spawn. ");
	}
}
