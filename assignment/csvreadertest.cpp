#include <unordered_map>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <math.h>

const int GRID_SIZE = 100;
const char CSV_DELIM = ',';

struct CSVLine {
  int row;
  int col;
  int num;
  double arm_shoulder_pan;
  double arm_elbow_flex;
  double arm_wrist_flex;
  double arm_shoulder_lift;
  double gripper;
};

// This is global so that we don't have to rebuild it over and over again
std::vector<std::vector<std::vector<CSVLine>>> CSVArray;


void buildCSVArray() {
  // Build the indices for the array
  for (int row = 0; row < GRID_SIZE; row++) {
    std::vector<std::vector<CSVLine>> tmpvv;
    CSVArray.push_back(tmpvv);
    for (int col = 0; col < GRID_SIZE; col++) {
      std::vector<CSVLine> tmpv;
      CSVArray[row].push_back(tmpv);
      for (int num = 0; num < GRID_SIZE; num++) {
        CSVLine tmp;
        CSVArray[row][col].push_back(tmp);
      }
    }
  }

  // Grab the file stream
  std::ifstream csvf;
  csvf.open("param.csv");
  // Skip the first line
  std::string s;
  std::getline(csvf, s);

  // This does not use constants because the file is static
  for (int i = 0; i < 1000000; i++) {
    CSVLine l;

    if (std::getline(csvf, s, CSV_DELIM)) {
      //std::cout << "THIS IS MY S " << s << std::endl;
      l.row = std::stoi(s);
    }

    if (std::getline(csvf, s, CSV_DELIM)) {
      //std::cout << "THIS IS MY S2 " << s << std::endl;
      l.col = std::stoi(s);
    }

    if (std::getline(csvf, s, CSV_DELIM)) {
      //std::cout << "THIS IS MY S3 " << s << std::endl;
      l.num = std::stoi(s);
    }

    if (std::getline(csvf, s, CSV_DELIM)) {
      //std::cout << "THIS IS MY S4 " << s << std::endl;
      l.arm_shoulder_pan = std::stod(s);
    }

    if (std::getline(csvf, s, CSV_DELIM)) {
      //std::cout << "THIS IS MY S5 " << s << std::endl;
      l.arm_elbow_flex = std::stod(s);
    }

    if (std::getline(csvf, s, CSV_DELIM)) {
      //std::cout << "THIS IS MY S6 " << s << std::endl;
      l.arm_wrist_flex = std::stod(s);
    }

    if (std::getline(csvf, s, CSV_DELIM)) {
      //std::cout << "THIS IS MY S7 " << s << std::endl;
      l.arm_shoulder_lift = std::stod(s);
    }

    if (std::getline(csvf, s, '\n')) {
      //std::cout << "THIS IS MY S8 " << s << std::endl;
      l.gripper = std::stod(s);
    }

    CSVArray[l.row - 1][l.col - 1][l.num - 1] = l;
  }

  csvf.close();
}


int main(int argc, char const *argv[]) {

  buildCSVArray();

  /*
  for (int row = 0; row < GRID_SIZE; row++) {
    for (int col = 0; col < GRID_SIZE; col++) {
      for (int num = 0; num < GRID_SIZE; num++) {
        continue;
      }
    }
  }
  */

  CSVLine test = CSVArray[100-1][100-1][100-1];

  std::cout << test.row << ' ' << test.col << ' ' << test.num << ' ' << std::endl;
  std::cout << test.arm_shoulder_pan << ' ' << test.arm_elbow_flex << ' ' << test.arm_wrist_flex << ' ' << test.arm_shoulder_lift << ' ' << test.gripper << std::endl;

  return 0;
}
