#include <ros/ros.h>
#include <cust_msg_pub/my_first_cust_msg.h>
#include <stdlib.h>
#include <iomanip>


void addAllParams(const cust_msg_pub::my_first_cust_msg& msg)
{
	ROS_INFO_STREAM("All params added: " << msg.A + msg.B + msg.C);
}


int main(int argc, char **argv)
{
	ros::init(argc, argv, "sub_to_cust");

	ros::NodeHandle nh;

	ros::Subscriber sub = nh.subscribe("cust_msg_pub_node/out", 1000, addAllParams);

	ros::spin();
}
